# Sand Format
Hi! Welcome to **Sand** specification!
**Sand** is the main format used [SandBoxes](https://underscorestudios.net/project/sandboxes) for storing saves.

# Legacy Sand
**Legacy Sand** or simply `.sand` is the format used since, saving in SandBoxes was implemented, up to version 43.

## Format Breakdown
**Legacy Sand** is quite bad of a format. It's made out of multiple arrays serialized into binary data. All of the arrays are separate and they should be used as a table.

Here is the class:
```csharp
[System.Serializable]
public class MapData
{
 public string Scene;
 public string GameVersion;
 public string[] ObjectNames;
 public bool[] ObjectStaticStates;
 public float[] ObjectPosX;
 public float[] ObjectPosY;
 public float[] ObjectPosZ;
 public float[] ObjectRotX;
 public float[] ObjectRotY;
 public float[] ObjectRotZ;
 public float[] ObjectRotW;
}
```


## Bugs
In all beta builds of version 40 a saving bug was present. Trying to save with objects placed by other players present, will generate null fields in the save file. Trying to load will result in an exception. 
Release 40 has implemented a solution to those. Loading a map with null fields in release 40 will spawn static **error** objects in the center of the map. It's the only way they can be spawned. Also there is a way to fix saves with null fields. Every version from 40 to 43 has a built-in fixing tool. It can be accessed by clicking `left alt + f` in the main menu.

# Sand2
**Sand2** is the future format for SandBoxes. It's using a `.sand2` extension and it will be used starting from the first version of **SandBoxes rewritten**.

## Format Breakdown
**Sand2** holds more information than just objects. It also holds player positions.

Here is the player table:
```sql
TABLE "PLAYERS" (
	"UserId"	TEXT NOT NULL UNIQUE,
	"X"	NUMERIC NOT NULL,
	"Y"	NUMERIC NOT NULL,
	"Z"	NUMERIC NOT NULL,
	PRIMARY KEY("UserId")
);
```

And here is the object table:
```sql
TABLE "OBJECTS" (
	"Id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"ObjectId"	TEXT NOT NULL,
	"Owner"	TEXT,
	"State"	INTEGER,
	"CreationDate"	INTEGER,
	"Health"	INTEGER,
	"Content"	TEXT,
	"PosX"	NUMERIC NOT NULL DEFAULT 0,
	"PosY"	NUMERIC NOT NULL DEFAULT 0,
	"PosZ"	NUMERIC NOT NULL DEFAULT 0,
	"RotX"	NUMERIC,
	"RotY"	NUMERIC,
	"RotZ"	NUMERIC,
	"RotW"	NUMERIC,
	"Scale"	NUMERIC NOT NULL DEFAULT 1.0
);
```

# Tools
Tools for converting, viewing and editing various **Sand** formats will be published in the near future. Stay tuned.